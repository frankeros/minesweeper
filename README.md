# Minesweeper

Minesweeper is a Xamarin project for a technical test of Kukuroo company.

## Installation

Clone the master repo in your local.

```bash
git clone https://frankeros@bitbucket.org/frankeros/minesweeper.git
```

## Usage

The solution was created with Visual Studio 2017 Community and Xamarin v3.4.0
For test purposes the project was implemented and tested in Minesweeper.UWP project, 
for this reason we request that the tests be carried out using this project.

## Notes
Only the requirements presented for the test have been covered, due to the proposed time.
But for the game to be complete some more importants functionalities are necessary, like:
    - Detect when the user win.
    - A label with the amount of flagged tiles and how many remain.
    - When a mine has exposed expose all mines in the board.
    - Also the user could define the size of the grid.

## License
[MIT](https://choosealicense.com/licenses/mit/)