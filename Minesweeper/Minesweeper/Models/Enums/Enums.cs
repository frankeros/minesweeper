﻿namespace Minesweeper.Models.Enums
{
    public class Enums
    {
        public enum GameStatus
        {
            Paused,
            Started,
            Ended,
        }

        public enum TileStatus
        {
            Hidden,
            Flagged,
            Exposed
        }

        public enum FlagType
        {
            Question,
            RedFlag,
            Mine,
            None
        }
    }
}
