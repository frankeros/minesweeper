﻿using Minesweeper.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Minesweeper.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TileView : ContentView
    {
        #region Attributes
        private readonly TileViewModel _tileViewModel;
        private bool _lastTapSingle;
        private DateTime _lastTapTime;
        #endregion

        #region Properties

        #endregion

        #region Constructor
        public TileView(TileViewModel tileViewModel)
        {
            InitializeComponent();

            _tileViewModel = tileViewModel;
            BindingContext = _tileViewModel;

            var singleTap = new TapGestureRecognizer
            {
                NumberOfTapsRequired = 1
            };
            singleTap.Tapped += OnSingleTap;
            GestureRecognizers.Add(singleTap);

            var doubleTap = new TapGestureRecognizer
            {
                NumberOfTapsRequired = 2
            };
            doubleTap.Tapped += OnDoubleTap;
            GestureRecognizers.Add(doubleTap);
        }
        #endregion

        #region Custom Methods
        void OnSingleTap(object sender, object args)
        {
            if (Device.RuntimePlatform == Device.UWP)
            {
                if (_lastTapSingle && DateTime.Now - _lastTapTime < TimeSpan.FromMilliseconds(400))
                {
                    OnDoubleTap(sender, args);
                    _lastTapSingle = false;
                }
                else
                {
                    _lastTapTime = DateTime.Now;
                    _lastTapSingle = true;
                }
            }

            _tileViewModel.FlagCommand.Execute(null);
        }

        private void OnDoubleTap(object sender, object args)
        {
            _tileViewModel.ExposeCommand.Execute(null);
        }
        #endregion
    }
}