﻿using Minesweeper.ViewModels;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Minesweeper.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BoardView : ContentView
    {
        #region Attributes
        //TODO: This values can be setted by the user;
        private int columns = 10;
        private int rows = 10;
        private int mines = 15;
        private BoardViewModel _boardViewModel;
        #endregion

        #region Properties
        #endregion

        #region Constructor
        public BoardView()
        {
            InitializeComponent();

            _boardViewModel = new BoardViewModel(columns, rows, mines);
            BindingContext = _boardViewModel;

            DefineBoardGrid();
        }
        #endregion


        #region Custom Methods
        private void DefineBoardGrid()
        {
            for (int r = 0; r < _boardViewModel.RowsCount; r++)
            {
                boardGrid.RowDefinitions.Add(new RowDefinition());
            }
            for (int c = 0; c < _boardViewModel.ColumnsCount; c++)
            {
                boardGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            for (int c = 0; c < _boardViewModel.ColumnsCount; c++)
            {
                for (int r = 0; r < _boardViewModel.RowsCount; r++)
                {
                    boardGrid.Children.Add(new TileView(_boardViewModel.Tiles[c, r]), c, r);
                }
            }
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            double dimension = Math.Min(width, height);
            double horzPadding = (width - dimension) / 2;
            double vertPadding = (height - dimension) / 2;
            Padding = new Thickness(horzPadding, vertPadding);
        }
        #endregion


    }
}