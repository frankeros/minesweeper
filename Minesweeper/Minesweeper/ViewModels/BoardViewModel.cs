﻿using Minesweeper.ViewModels.Base;
using System;
using System.Windows.Input;
using static Minesweeper.Models.Enums.Enums;

namespace Minesweeper.ViewModels
{
    public class BoardViewModel : ViewModelBase
    {
        #region Attributes
        private int _columnsCount;
        private int _rowsCount;
        private int _minesCount;
        private TileViewModel[,] _tiles;
        private bool _isGameOver;
        private DelegateCommand _gameStartCommand;
        #endregion

        #region Properties
        public int ColumnsCount
        {
            get { return _columnsCount; }
        }
        public int RowsCount
        {
            get { return _rowsCount; }
        }
        public int MinesCount
        {
            get { return _minesCount; }
        }
        public TileViewModel[,] Tiles
        {
            get { return _tiles; }
        }
        public event EventHandler GameStarted;
        public event EventHandler GameOver;
        public ICommand GameStartCommand
        {
            get { return _gameStartCommand = _gameStartCommand ?? new DelegateCommand(GameStart); }

        }
        #endregion

        #region Constructor
        public BoardViewModel(int columns, int rows, int mines)
        {
            CreateNewBoard(columns, rows, mines);
        }
        #endregion

        #region Custom Methods
        private void CreateNewBoard(int columnsCount, int rowsCount, int minesCount)
        {
            _columnsCount = columnsCount;
            _rowsCount = rowsCount;
            _minesCount = minesCount;

            _tiles = new TileViewModel[_columnsCount, _rowsCount];
            for (int c = 0; c < _columnsCount; c++)
            {
                for (int r = 0; r < _rowsCount; r++)
                {
                    var tileViewModel = new TileViewModel(c, r);
                    tileViewModel.PropertyChanged += OnTileStatusChanged;
                    _tiles[c, r] = tileViewModel;
                }
            }

            var random = new Random();
            for (int m = 0; m < _minesCount; m++)
            {
                int col = random.Next(_columnsCount);
                int row = random.Next(_rowsCount);

                if (_tiles[col, row].HasMine)
                    continue;

                GetAdjacentsTiles(col, row, (adjCol, adjRow) =>
                {
                    ++_tiles[adjCol, adjRow].AdjacentMinesCount;
                });

                _tiles[col, row].HasMine = true;
            }
        }

        private void OnTileStatusChanged(object sender, EventArgs args)
        {
            var tileViewModel = (TileViewModel)sender;
            switch (tileViewModel.TileStatus)
            {
                case TileStatus.Hidden:
                    break;
                case TileStatus.Flagged:
                    break;
                case TileStatus.Exposed:
                    if (_isGameOver)
                        return;

                    if (tileViewModel.HasMine)
                    {
                        _isGameOver = true;
                        GameOver?.Invoke(this, EventArgs.Empty);
;                    }

                    if (tileViewModel.AdjacentMinesCount == 0)
                        GetAdjacentsTiles(tileViewModel.ColIndex, tileViewModel.RowIndex, (adjCol, adjRow) =>
                        {
                            var _tileAdjacent = _tiles[adjCol, adjRow];
                            if (_tileAdjacent.TileStatus != TileStatus.Flagged && !_tileAdjacent.HasMine)
                                _tileAdjacent.ExposeCommand.Execute(null);
                        });
                    break;
            }
        }

        private void GetAdjacentsTiles(int centerColumn, int centerRow, Action<int, int> callback)
        {
            int minRow = Math.Max(0, centerRow - 1);
            int maxRow = Math.Min(_rowsCount - 1, centerRow + 1);
            int minCol = Math.Max(0, centerColumn - 1);
            int maxCol = Math.Min(_columnsCount - 1, centerColumn + 1);

            for (int adjRow = minRow; adjRow <= maxRow; adjRow++)
                for (int adjCol = minCol; adjCol <= maxCol; adjCol++)
                {
                    if (adjRow != centerRow || adjCol != centerColumn)
                        callback(adjCol, adjRow);
                }
        }

        private void GameStart()
        {
            GameStarted?.Invoke(this, EventArgs.Empty);
        }
        #endregion
    }
}
