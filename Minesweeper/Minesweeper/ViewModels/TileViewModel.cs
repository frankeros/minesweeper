﻿using Minesweeper.ViewModels.Base;
using System;
using System.Windows.Input;
using static Minesweeper.Models.Enums.Enums;

namespace Minesweeper.ViewModels
{
    public class TileViewModel : ViewModelBase
    {
        #region Attributes
        private readonly int _colIndex;
        private readonly int _rowIndex;
        private bool _hasMine;
        private TileStatus _tileStatus;
        private FlagType _flagType;
        private DelegateCommand _flagCommand;
        private DelegateCommand _exposeCommand;
        private int _adjacentMinesCount;
        #endregion

        #region Properties
        public int ColIndex
        {
            get { return _colIndex; }
        }
        public int RowIndex
        {
            get { return _rowIndex; }
        }
        public bool HasMine
        {
            get { return _hasMine; }
            set
            {
                _hasMine = value;
                //uncomment this line to easy game over test
                //FlagType = FlagType.Mine; 
            }
        }
        public TileStatus TileStatus
        {
            get { return _tileStatus; }
            set
            {
                if (_tileStatus != value)
                {
                    _tileStatus = value;
                    RaisePropertyChanged();
                }
            }
        }
        public FlagType FlagType
        {
            get { return _flagType; }
            set
            {
                if (_flagType != value)
                {
                    _flagType = value;
                    RaisePropertyChanged();

                }
            }
        }
        public int AdjacentMinesCount
        {
            get { return _adjacentMinesCount; }
            set { _adjacentMinesCount = value; }
        }

        public ICommand FlagCommand
        {
            get { return _flagCommand = _flagCommand ?? new DelegateCommand(SetFlag); }
        }
        public ICommand ExposeCommand
        {
            get { return _exposeCommand = _exposeCommand ?? new DelegateCommand(Expose); }
        }
        public event EventHandler TileStatusChanged;
        #endregion

        #region Constructor
        public TileViewModel(int colIndex, int rowIndex)
        {
            _colIndex = colIndex;
            _rowIndex = rowIndex;
            _flagType = FlagType.None;
            _tileStatus = TileStatus.Hidden;
        }
        #endregion

        #region Custom Methods
        private void SetFlag()
        {
            switch (_tileStatus)
            {
                case TileStatus.Hidden:
                    TileStatus = TileStatus.Flagged;
                    FlagType = FlagType.RedFlag;
                    break;
                case TileStatus.Flagged:
                    if (_flagType == FlagType.RedFlag)
                        FlagType = FlagType.Question;
                    else
                    {
                        TileStatus = TileStatus.Hidden;
                        FlagType = FlagType.None;
                    }
                    break;
                case TileStatus.Exposed:
                    break;
            }
            TileStatusChanged?.Invoke(this, EventArgs.Empty);
        }

        private void Expose()
        {
            TileStatus = TileStatus.Exposed;
            FlagType = _hasMine ? FlagType.Mine : FlagType.None;
            TileStatusChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion
    }
}
