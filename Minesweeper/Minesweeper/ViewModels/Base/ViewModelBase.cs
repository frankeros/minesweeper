﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Minesweeper.ViewModels.Base
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
