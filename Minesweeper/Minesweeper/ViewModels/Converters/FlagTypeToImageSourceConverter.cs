﻿using System;
using System.Globalization;
using Xamarin.Forms;
using static Minesweeper.Models.Enums.Enums;

namespace Minesweeper.ViewModels.Converters
{
    public class FlagTypeToImageSourceConverter : IValueConverter
    {
        private static readonly ImageSource redFlagImageSource = ImageSource.FromFile("red_flag.png");
        private static readonly ImageSource questionImageSource = ImageSource.FromFile("question_mark.png");
        private static readonly ImageSource mineImageSource = ImageSource.FromFile("mine.png");

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is FlagType && value != null)
            {
                var flagType = (FlagType)value;
                switch (flagType)
                {
                    case FlagType.RedFlag:
                        return redFlagImageSource;
                    case FlagType.Question:
                        return questionImageSource;
                    case FlagType.Mine:
                        return mineImageSource;
                    case FlagType.None:
                        return null;
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
