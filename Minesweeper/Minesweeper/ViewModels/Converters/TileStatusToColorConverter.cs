﻿using System;
using System.Globalization;
using Xamarin.Forms;
using static Minesweeper.Models.Enums.Enums;

namespace Minesweeper.ViewModels.Converters
{
    public class TileStatusToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is TileStatus && value != null)
            {
                var tileStatus = (TileStatus)value;
                switch (tileStatus)
                {
                    case TileStatus.Hidden:
                        return Color.Gray;
                    case TileStatus.Flagged:
                        return Color.LightGray;
                    case TileStatus.Exposed:
                        return Color.WhiteSmoke;
                }
            }

            return Color.Gray;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
