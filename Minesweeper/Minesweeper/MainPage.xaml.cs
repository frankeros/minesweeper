﻿using Minesweeper.ViewModels;
using System;
using Xamarin.Forms;

namespace Minesweeper
{
    public partial class MainPage : ContentPage
    {
        #region Attributes
        private const string _timeFormat = @"%m\:ss";
        private bool _isGameInProgress;
        private DateTime _gameStartTime;
        private BoardViewModel _boardViewModel;
        #endregion

        #region Constructor
        public MainPage()
        {
            InitializeComponent();

            gameOverText.IsVisible = false;
            board.IsEnabled = false;
            _boardViewModel = (BoardViewModel)board.BindingContext;
            _boardViewModel.GameStarted += OnGameStarted;
            _boardViewModel.GameOver += OnGameOver;
        }
        #endregion

        #region Custom Methods
        private void OnGameStarted(object sender, EventArgs e)
        {
            _isGameInProgress = true;
            board.IsEnabled = true;
            gameOverText.IsVisible = false;
            playButton.IsVisible = false;
            _gameStartTime = DateTime.Now;

            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                timeLabel.Text = (DateTime.Now - _gameStartTime).ToString(_timeFormat);
                return _isGameInProgress;
            });
        }

        private void OnGameOver(object sender, EventArgs e)
        {
            _isGameInProgress = false;
            board.IsEnabled = false;
            gameOverText.IsVisible = true;
            playButton.IsVisible = true;
        }

        private void PlayButton_Clicked(object sender, EventArgs e)
        {
            _boardViewModel.GameStartCommand.Execute(null);
        }
        #endregion


    }
}
